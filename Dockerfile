FROM ubuntu:18.04

RUN apt-get update && apt-get install -y \
    build-essential \
    pkg-config \
    git \
    python3 \
    python3-pip \
    openssh-client && \
    pip3 install hvac && \
    pip3 install argparse && \
    pip3 install PyYAML && \
    pip3 install requests && \
    pip3 install ansible && \
    apt-get -y remove build-essential && \
    apt-get clean -y && \
    apt-get autoremove -y && \
    apt-get install -y puppet && \
    apt-get install -y puppet-lint && \
    rm -rf /var/lib/apt/lists/* 
