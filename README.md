# deployer-base

This project creates a base image that will be stored in the projects container registry. This base image will be used by the PCS project's CI/CD process.
By installing software in the base image the CI pipeline to deploy new code will be quicker as there will be less to do there.
